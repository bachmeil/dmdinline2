/* 
	Example of a standalone file that you should be able to use with dub
	This is how I compile using a Makefile:
	dmd -c standalone.d -fPIC -I/~/repos/rtod2/
	dmd -oflibstandalone.so standalone.o -L/usr/lib/libgretl-1.0.so -L/usr/lib/R/lib/libR.so -L/usr/lib/librtod2.so -shared -defaultlib=libphobos2.so
*/

import rtod2.matrix2, rtod2.utils2;

mixin(genRLib(q{
	Robj println(Robj rm) {
		auto m = RMatrix(rm);
		print(m, "Matrix from R");
		return RNil;
	}
}, "ex1"));

/*
	Send a matrix from R to D. Will print it in D and return NULL
	Example R code:
	
	dyn.load("libstandalone.so")
	z <- matrix(rnorm(20), ncol=4)
	.Call("println", z)
*/