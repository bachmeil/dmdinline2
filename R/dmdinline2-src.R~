utils::globalVariables(c("rtod2src", "librtod2", "gretl", "libr"))

reload.dmdinline <- function() {
	detach("package:dmdinline", unload = TRUE)
  library(dmdinline)
}

d_code <- function(code, libname, pre="") {
	paste('import core.runtime;
import rtod2.utils, rtod2.r, rtod2.vector, rtod2.matrix, rtod2.reg, rtod2.gretl, rtod2.random, rtod2.dynmodel;
struct DllInfo;
', pre, '
extern (C) {
  void R_init_lib', libname, '(DllInfo * info) {
    Runtime.initialize();
    gretl_rand_init();
  }

  void R_unload_lib', libname, '(DllInfo * info) {
    gretl_rand_free();
  }
', code, '\n}\n', sep="")
}

d_file <- function(code, libname, pre="") {
	cat(d_code(code, libname, pre), file=paste("lib", libname, ".d", sep=""))
}

compileD <- function(libname, code, config="~/.rtod2/config.R", pre="", file=FALSE) {
  source(config)
  if (file) {
		program <- paste(readLines(code), collapse="\n")
	} else {
		program <- d_code(code, libname, pre)
		print(strsplit(program, "\n", fixed=TRUE))
		cat(program, file=paste('lib', libname, '.d', sep=''))
	}
  
  cmd1 <- paste("dmd -c lib", libname, ".d -fPIC -I/", rtod2src, " -L/", librtod2, " -L/", gretl, " -L/", libr, sep="")
  out1 <- system(cmd1, intern=TRUE)
  print(cmd1)
  if (is.character(out1) & (length(out1) == 0)) {
    print("Compilation succeeded")
  } else {
    print(out1)
  }
  cmd2 <- paste("dmd -oflib", libname, ".so lib", libname, ".o -release -shared -defaultlib=libphobos2.so ",  "-I/", rtod2src, " -L/", librtod2, " -L/", gretl, " -L/", libr, sep="")
  out2 <- system(cmd2, intern=TRUE)
  print(cmd2)
  if (is.character(out2) & (length(out2) == 0)) {
    print("Compilation succeeded")
  } else {
    print(out2)
  }
  dyn.load(paste("lib", libname, ".so", sep=""))
}

install_librtod2 <- function(gretl="usr/lib/libgretl-1.0.so", libr="usr/lib/R/lib/libR.so", user="") {
  rtod2 <- paste(find.package("dmdinline"), "/rtod2", sep="")
  names <- c("gretl", "matrix", "r", "random", "reg", "utils", "vector", "dynmodel")
  for (name in names) {
    code <- paste('cd ', rtod2, '; dmd -c ', name, '.d -fPIC -w -I/', find.package("dmdinline"), ' -L/', gretl, ' -L/', libr, sep="")
    print(code)
    messages <- system(code, intern=TRUE)
    print(messages)
  }
  code <- paste('cd ', rtod2, '; dmd -oflibrtod2.so ', paste(names, '.o', sep="", collapse=" "), ' -release -inline -shared -defaultlib=libphobos2.so -I/', find.package("dmdinline"), ' -L/', gretl, ' -L/', libr, sep="")
  print(code)
  messages <- system(code, intern=TRUE)
  print(messages)
  config <- paste('rtod2src <- "', find.package("dmdinline"), '"
librtod2 <- "', rtod2, '/librtod2.so"
gretl <- "', gretl, '"
libr <- "', libr, '"', sep="")
	if (user=="") {
		system("mkdir -p ~/.rtod2")
		cat(config, file="~/.rtod2/config.R")
	} else {
		system(paste("mkdir -p /home/", user, "/.rtod2", sep=""))
		cat(config, file=paste("/home/", user, "/.rtod2/config.R", sep=""))
	}
}
